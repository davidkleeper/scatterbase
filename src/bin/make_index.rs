// cargo run --bin make_index ..\hero-core\database\windows\archetypes.json -o ..\hero-core\database\windows\archetypes.index -q $.id
// cargo run --bin make_index ..\hero-core\database\windows\enhancement-sets.json -o ..\hero-core\database\windows\enhancement-sets.index -q $.id
// cargo run --bin make_index ..\hero-core\database\windows\enhancements.json -o ..\hero-core\database\windows\enhancements.index -q $.id
// cargo run --bin make_index ..\hero-core\database\windows\power-sets.json -o ..\hero-core\database\windows\power-sets.index -q $.id
// cargo run --bin make_index ..\hero-core\database\windows\powers.json -o ..\hero-core\database\windows\powers.index -q $.id

// cargo run --bin make_index ../hero-core/database/unix/archetypes.json -o ../hero-core/database/unix/archetypes.index -q $.id
// cargo run --bin make_index ../hero-core/database/unix/enhancement-sets.json -o ../hero-core/database/unix/enhancement-sets.index -q $.id
// cargo run --bin make_index ../hero-core/database/unix/enhancements.json -o ../hero-core/database/unix/enhancements.index -q $.id
// cargo run --bin make_index ../hero-core/database/unix/power-sets.json -o ../hero-core/database/unix/power-sets.index -q $.id
// cargo run --bin make_index ../hero-core/database/unix/powers.json -o ../hero-core/database/unix/powers.index -q $.id
// cargo run --bin make_index ../hero-core/database/unix/player_archetypes.json -o ../hero-core/database/unix/player_archetypes.index -q $.id
// cargo run --bin make_index ../hero-core/database/unix/player_effects.json -o ../hero-core/database/unix/player_effects.index -q $.id
// cargo run --bin make_index ../hero-core/database/unix/player_power_sets.json -o ../hero-core/database/unix/player_power_sets.index -q $.id
// cargo run --bin make_index ../hero-core/database/unix/player_powers.json -o ../hero-core/database/unix/player_powers.index -q $.id
extern crate getopts;
use std::fs::File;
use getopts::Options;
use std::env;

extern crate scatterbase;
use scatterbase::functions::disk::index;
use scatterbase::functions::index::sort;
use scatterbase::functions::index::write;
use scatterbase::enums::sort_direction::SortDirection;
use scatterbase::enums::sort_value::SortValue;

fn do_work(inp: &str, key: &str, out: Option<String>) {
    let output = match out {
        Some(x) => x,
        None => String::from("output.index"),
    };

    let mut json = File::open(inp).unwrap();
    let index_data = index(&mut json, key).unwrap();
    let new_index = sort(&index_data, SortValue::Character, SortDirection::Ascending);
    let result = match write(output.as_str(), &new_index){
        Ok(_s) => 0,
        Err(_e) => 1
    };
    println!("Query {}", key);
    println!("{}", result);
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} FILE [options]", program);
    print!("{}", opts.usage(&brief));
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.reqopt("q", "", "set json query used to build index", "$.enhance.id");
    opts.optopt("o", "", "set output file name", "NAME");
    opts.optflag("h", "help", "print this help menu");  
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(_f) => { print_usage(&program, opts); return; }
    };
    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }
    let output = matches.opt_str("o");
    let key = match matches.opt_str("q") {
        Some(k) => k.clone(),
        None => {
            print_usage(&program, opts);
            return;
        }
    };
    let input = if !matches.free.is_empty() {
        matches.free[0].clone()
    } else {
        print_usage(&program, opts);
        return;
    };
    do_work(&input, &key, output);
}