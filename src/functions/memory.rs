use serde_json::Value;

pub fn index(json: &str, key_path: &str) -> Result<Vec::<(String, u64, u64)>, std::io::Error>{
    let mut layout = Vec::<(String, u64, u64)>::new();
    let trimed = json.trim();
    let chars_count: usize = trimed.chars().count();
    let character = match trimed.chars().nth(0) {
        Some(c) => c,
        None => return Ok(layout)
    };
    if '[' != character && '{' != character { return Ok(layout); }
    let mut current_character: usize = 1;
    let mut brace_count: usize = 0;
    let mut record_start: usize = 0;
    if '{' == character { 
        brace_count = 1; 
    }
    while current_character < chars_count {
        let character = match trimed.chars().nth(current_character) {
            Some(c) => c,
            None => return Ok(layout)
        };
        if '{' == character { 
            brace_count += 1;
            if 1 == brace_count { record_start = current_character }
        }
        else if '}' == character { 
            brace_count -= 1; 
            if 0 == brace_count {
                let length = current_character - record_start + 1;
                let rec: String = trimed.chars().skip(record_start).take(length).collect();
                let json_object =  serde_json::from_str(rec.as_str()).unwrap();
                let mut selector = jsonpath::selector(&json_object);
                let keys = selector(key_path).unwrap();
                for keys_index in 0..keys.len() {
                    let key = match keys[keys_index] {
                        Value::Number(n) => format!("{}", n),
                        Value::String(s) => format!("{}", s),
                        Value::Bool(b) => format!("{}", b),
                        _ => String::from("")
                    };
                    layout.push((key.to_string(), record_start as u64, current_character as u64));
                }
            }
       }
       current_character += 1;
    }
    Ok(layout)
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use crate::functions::memory::index;
   
    #[test]
    fn test_index() {
        let mut json = "";
        let mut rec_index = index(json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 0);

        json = "{}";
        rec_index = index(json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 0);

        json = "[]";
        rec_index = index(json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 0);

        json = r#"{ "id": 14, "sub_id": -1 }"#;
        rec_index = index(json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 1);
        assert_eq!(rec_index[0].0, "14");
        assert_eq!(rec_index[0].1, 0);
        assert_eq!(rec_index[0].2, json.len() as u64 - 1);

        json = r#"[{ "id": 14, "sub_id": -1 }]"#;
        rec_index = index(json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 1);
        assert_eq!(rec_index[0].0, "14");
        assert_eq!(rec_index[0].1, 1);
        assert_eq!(rec_index[0].2, json.len() as u64 - 2);

        json = r#"[{ "id": 14, "sub_id": -1 }, { "id": 15, "sub_id": -1 }, { "id": 16, "sub_id": -1 }]"#;
        rec_index = index(json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 3);
        assert_eq!(rec_index[0].0, "14");
        assert_eq!(rec_index[0].1, 1);
        assert_eq!(rec_index[0].2, 26);
        assert_eq!(rec_index[1].0, "15");
        assert_eq!(rec_index[1].1, 29);
        assert_eq!(rec_index[1].2, 54);
        assert_eq!(rec_index[2].0, "16");
        assert_eq!(rec_index[2].1, 57);
        assert_eq!(rec_index[2].2, json.len() as u64 - 2);

        json = r#"{ "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 14, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null }"#;
        rec_index = index(json, "$.enhance.id").unwrap();
        assert_eq!(rec_index.len(), 1);
        assert_eq!(rec_index[0].0, "14");
        assert_eq!(rec_index[0].1, 0);
        assert_eq!(rec_index[0].2, json.len() as u64 - 1);

        json = r#"[
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 14, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 15, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 16, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            ]"#;
        rec_index = index(json, "$.enhance.id").unwrap();
        assert_eq!(rec_index.len(), 3);
        assert_eq!(rec_index[0].0, "14");
        assert_eq!(rec_index[0].1, 14);
        assert_eq!(rec_index[0].2, 145);
        assert_eq!(rec_index[1].0, "15");
        assert_eq!(rec_index[1].1, 160);
        assert_eq!(rec_index[1].2, 291);
        assert_eq!(rec_index[2].0, "16");
        assert_eq!(rec_index[2].1, 306);
        assert_eq!(rec_index[2].2, 437);
    }

}
