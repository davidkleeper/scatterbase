use std::fs::File;
use std::io::prelude::*;
use std::io::SeekFrom;
use crate::functions::index::find;
use crate::poro::index::Index;
use crate::poro::registry::Registry;

pub fn has_key(registry: &Registry, key: &str) -> bool{
    has_disk_key(registry, key) || has_memory_key(registry, key)
}

pub fn has_disk_key(registry: &Registry, key: &str) -> bool{
    registry.disk.contains_key(key)
}

pub fn has_memory_key(registry: &Registry, key: &str) -> bool{
    registry.memory.contains_key(key)
}

pub fn write(file: &str, registry: &Registry) -> Result<i32, std::io::Error>{
    let serialized_registry = serde_json::to_string(registry).unwrap();
    std::fs::write(file, serialized_registry)?;
    Ok(0)
}

pub fn read(file: &str) -> Result<Registry, std::io::Error>{
    let serialized_registry = String::from_utf8(std::fs::read(file).unwrap()).unwrap();
    let registry: Registry = serde_json::from_str(&serialized_registry).unwrap();
    Ok(registry)
}

pub fn register_from_disk_to_disk(registry: &Registry, key: &str, data_file: &str, index_file: &str) -> Result<Registry, std::io::Error>{
    let mut clone = registry.clone();
    if has_key(registry, key) { return Ok(clone); }

    let index_data = std::fs::read_to_string(index_file)?;

    clone.disk.insert(key.to_string().clone(), (data_file.to_string().clone(), index_data.clone()));
    Ok(clone)
}

pub fn register_from_disk_to_memory(registry: &Registry, key: &str, data_file: &str, index_file: &str) -> Result<Registry, std::io::Error>{
    let mut file_data = File::open(data_file)?;
    let mut data = String::new();
    file_data.read_to_string(&mut data)?;
    let mut file_index = File::open(index_file)?;
    let mut index = String::new();
    file_index.read_to_string(&mut index)?;
    register_from_memory_to_memory(registry, key, &data, &index)
}

pub fn register_from_memory_to_memory(registry: &Registry, key: &str, data: &str, index: &str) -> Result<Registry, std::io::Error>{
    let mut clone = registry.clone();
    if has_key(registry, key) { return Ok(clone); }

    clone.memory.insert(key.to_string().clone(), (data.to_string().clone(), index.to_string().clone()));
    Ok(clone)
}

pub fn get_index(registry: &Registry, key: &str) -> Result<Option<Index>, std::io::Error>{
    if !has_key(registry, key) { return Ok(None); }
    let index_str = match registry.memory.get(key) {
        Some(index) => &index.1,
        None => {
            match registry.disk.get(key) {
                Some(index) => &index.1,
                None => return Ok(None)
            }
        }
    };
    let index: Index = serde_json::from_str(index_str)?;
    Ok(Some(index))
}

pub fn get_data(registry: &Registry, key: &str) -> Result<Option<String>, std::io::Error>{
    if !has_key(registry, key) { return Ok(None); }
    let data_str = match registry.memory.get(key) {
        Some(index) => &index.0,
        None => {
            match registry.disk.get(key) {
                Some(index) => &index.0,
                None => return Ok(None)
            }
        }
    };
    Ok(Some(String::from(data_str)))
}

pub fn get_value(registry: &Registry, key: &str, query: &str) -> Result<Option<String>, std::io::Error>{
    let index = match get_index(registry, key)? {
        Some(i) => i,
        None => {println!("Index not found."); return Ok(None)}
    };
    let data = match get_data(registry, key)? {
        Some(d) => d,
        None => {println!("Data not found."); return Ok(None)}
    };
    let location = match find(query, &index)? {
        Some(l) => l,
        None => {println!("Location not found for {}.", query); return Ok(None)}
    };
    let length: usize = (location.1 - location.0 + 1) as usize;
    if has_memory_key(registry, key) {
        let start: usize = location.0 as usize;
        let record = data.chars().skip(start).take(length).collect();
        return Ok(Some(record))
    }

    let mut data_file = File::open(data)?;
    let mut buffer : Vec<u8> = Vec::with_capacity(length);
    for _ in 0..buffer.capacity() { buffer.push(0); }
    data_file.seek(SeekFrom::Start(location.0))?;
    data_file.read(&mut buffer[..]).unwrap();
    let data_str = match std::str::from_utf8(&buffer) {
        Ok(s) => s,
        Err(e) => return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, e))
    };

    Ok(Some(String::from(data_str)))
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use crate::enums::sort_direction::SortDirection;
    use crate::enums::sort_value::SortValue;
    use crate::functions::cross_platform::path;
    use crate::functions::disk;
    use crate::functions::index;
    use crate::poro::registry::Registry;

    #[test]
    fn test_has_key() {
        let mut registry = Registry::new();
        registry.disk.insert("My_File".to_string(), ("My_File.data".to_string(), "My_File.index".to_string()));
        assert_eq!(super::has_disk_key(&registry, "My_File"), true);
        assert_eq!(super::has_disk_key(&registry, "My_Other_File"), false);
        assert_eq!(super::has_memory_key(&registry, "My_File"), false);
        assert_eq!(super::has_key(&registry, "My_File"), true);
        assert_eq!(super::has_key(&registry, "My_Other_File"), false);

        registry.memory.insert("My_Data".to_string(), (r#"{ "id": 14, "sub_id": -1 }"#.to_string(), r#"[["14",0,25]]"#.to_string()));
        assert_eq!(super::has_memory_key(&registry, "My_Data"), true);
        assert_eq!(super::has_memory_key(&registry, "My_Other_Data"), false);
        assert_eq!(super::has_disk_key(&registry, "My_Data"), false);
        assert_eq!(super::has_key(&registry, "My_Data"), true);
        assert_eq!(super::has_key(&registry, "My_Other_Data"), false);
    }

    #[test]
    fn test_read_and_write() {
        let mut registry = Registry::new();
        registry.disk.insert("My_File".to_string(), ("My_File.data".to_string(), "My_File.index".to_string()));
        registry.disk.insert("My_File2".to_string(), ("My_File2.data".to_string(), "My_File2.index".to_string()));
        registry.memory.insert("My_Data".to_string(), ("My_Data.data".to_string(), "My_Data.index".to_string()));
        registry.memory.insert("My_Data2".to_string(), ("My_Data2.data".to_string(), "My_Data2.index".to_string()));
        super::write(&path("./data/test_registry.json"), &registry).unwrap();
        let registry2 = super::read(&path("./data/test_registry.json")).unwrap();
        assert_eq!(registry.disk.get("My_File").unwrap(), registry2.disk.get("My_File").unwrap());
        assert_eq!(registry.disk.get("My_File2").unwrap(), registry2.disk.get("My_File2").unwrap());
        assert_eq!(registry.memory.get("My_Data").unwrap(), registry2.memory.get("My_Data").unwrap());
        assert_eq!(registry.memory.get("My_Data2").unwrap(), registry2.memory.get("My_Data2").unwrap());
    }

    #[test]
    fn test_register() {
        let mut registry = Registry::new();
        let mut file = File::open(&path("./data/simple-obj.json")).unwrap();
        let i = disk::index(&mut file, "$.id").unwrap();
        let index_poro = index::sort(&i, SortValue::Character, SortDirection::Ascending);
        index::write(&path("./data/simple-obj.index"), &index_poro).unwrap();
        let index_data = std::fs::read_to_string(&path("./data/simple-obj.index")).unwrap();
        let data_string = String::from_utf8(std::fs::read(&path("./data/simple-obj.json")).unwrap()).unwrap();
        let index_string = String::from_utf8(std::fs::read(&path("./data/simple-obj.index")).unwrap()).unwrap();

        registry = super::register_from_disk_to_disk(&registry, "Disk To Disk", &path("./data/simple-obj.json"), &path("./data/simple-obj.index")).unwrap();
        assert_eq!(super::has_disk_key(&registry, "Disk To Disk"), true);
        assert_eq!(super::has_memory_key(&registry, "Disk To Memory"), false);
        assert_eq!(super::has_memory_key(&registry, "Memory To Memory"), false);
        assert_eq!(super::has_key(&registry, "Disk To Disk"), true);
        assert_eq!(super::has_key(&registry, "Disk To Memory"), false);
        assert_eq!(super::has_key(&registry, "Memory To Memory"), false);

        let mut value = registry.disk.get("Disk To Disk").unwrap();
        assert_eq!(value.0 == path("./data/simple-obj.json"), true);
        assert_eq!(value.1 == index_data, true);

        registry = super::register_from_disk_to_memory(&registry, "Disk To Memory", &path("./data/simple-obj.json"), &path("./data/simple-obj.index")).unwrap();
        assert_eq!(super::has_disk_key(&registry, "Disk To Disk"), true);
        assert_eq!(super::has_memory_key(&registry, "Disk To Memory"), true);
        assert_eq!(super::has_memory_key(&registry, "Memory To Memory"), false);
        assert_eq!(super::has_key(&registry, "Disk To Disk"), true);
        assert_eq!(super::has_key(&registry, "Disk To Memory"), true);
        assert_eq!(super::has_key(&registry, "Memory To Memory"), false);

        value = registry.disk.get("Disk To Disk").unwrap();
        assert_eq!(value.0 == path("./data/simple-obj.json"), true);
        assert_eq!(value.1 == index_data, true);
        value = registry.memory.get("Disk To Memory").unwrap();
        assert_eq!(value.0 == data_string, true);
        assert_eq!(value.1 == index_string, true);

        registry = super::register_from_memory_to_memory(&registry, "Memory To Memory", &data_string, &index_string).unwrap();
        assert_eq!(super::has_disk_key(&registry, "Disk To Disk"), true);
        assert_eq!(super::has_memory_key(&registry, "Disk To Memory"), true);
        assert_eq!(super::has_memory_key(&registry, "Memory To Memory"), true);
        assert_eq!(super::has_key(&registry, "Disk To Disk"), true);
        assert_eq!(super::has_key(&registry, "Disk To Memory"), true);
        assert_eq!(super::has_key(&registry, "Memory To Memory"), true);

        value = registry.disk.get("Disk To Disk").unwrap();
        assert_eq!(value.0 == path("./data/simple-obj.json"), true);
        assert_eq!(value.1 == index_data, true);
        value = registry.memory.get("Disk To Memory").unwrap();
        assert_eq!(value.0 == data_string, true);
        assert_eq!(value.1 == index_string, true);
        value = registry.memory.get("Memory To Memory").unwrap();
        assert_eq!(value.0 == data_string, true);
        assert_eq!(value.1 == index_string, true);
    }
    
    #[test]
    fn test_get_index() {
        let mut registry = Registry::new();
        let mut file = File::open(&path("./data/simple-obj.json")).unwrap();
        let i = disk::index(&mut file, "$.id").unwrap();
        let index_poro = index::sort(&i, SortValue::Character, SortDirection::Ascending);
        index::write("./data/simple-obj.index", &index_poro).unwrap();
        let data_string = String::from_utf8(std::fs::read(&path("./data/simple-obj.json")).unwrap()).unwrap();
        let index_string = String::from_utf8(std::fs::read(&path("./data/simple-obj.index")).unwrap()).unwrap();
        registry = super::register_from_memory_to_memory(&registry, "Memory To Memory", &data_string, &index_string).unwrap();
        let index = super::get_index(&registry, "Memory To Memory").unwrap().unwrap();
        assert_eq!(index_poro.index, index.index);
        assert_eq!(index_poro.sort_direction, index.sort_direction);
        assert_eq!(index_poro.sort_value, index.sort_value);
    }
   
    #[test]
    fn test_get_value() {
        let mut registry = Registry::new();
        let mut file = File::open(&path("./data/simple-obj.json")).unwrap();
        let i = disk::index(&mut file, "$.id").unwrap();
        let index_poro = index::sort(&i, SortValue::Character, SortDirection::Ascending);
        index::write(&path("./data/simple-obj.index"), &index_poro).unwrap();
        let data_string = String::from_utf8(std::fs::read(&path("./data/simple-obj.json")).unwrap()).unwrap();
        let index_string = String::from_utf8(std::fs::read(&path("./data/simple-obj.index")).unwrap()).unwrap();
        registry = super::register_from_memory_to_memory(&registry, "Memory To Memory", &data_string, &index_string).unwrap();
        let mut value = super::get_value(&registry, "Memory To Memory", "14").unwrap().unwrap();
        assert_eq!(value, "{ \"id\": 14, \"sub_id\": -1 }");

        registry = super::register_from_disk_to_disk(&registry, "Disk To Disk", &path("./data/simple-obj.json"), &path("./data/simple-obj.index")).unwrap();
        value = super::get_value(&registry, "Disk To Disk", "14").unwrap().unwrap();
        assert_eq!(value, "{ \"id\": 14, \"sub_id\": -1 }");
    }
}
