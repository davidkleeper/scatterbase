use std::cmp::Ordering;
use crate::enums::sort_value::SortValue;
use crate::enums::sort_direction::SortDirection;
use crate::poro::index::Index;

pub fn sort(data: &Vec::<(String, u64, u64)>, sort_value: SortValue, sort_direction: SortDirection) -> Index{
    let mut clone = data.clone();

    if sort_direction == SortDirection::Ascending {
        if sort_value == SortValue::Character { clone.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap_or(Ordering::Equal)); }
        else { clone.sort_by(|a, b| a.0.parse::<f64>().unwrap().partial_cmp(&b.0.parse::<f64>().unwrap()).unwrap_or(Ordering::Equal)); } 
    } else {
        if sort_value == SortValue::Character { clone.sort_by(|a, b| b.0.partial_cmp(&a.0).unwrap_or(Ordering::Equal)); }
        else { clone.sort_by(|a, b| b.0.parse::<f64>().unwrap().partial_cmp(&a.0.parse::<f64>().unwrap()).unwrap_or(Ordering::Equal)); } 
    }
    let mut index = Index::new();
    index.index = clone;
    index.sort_direction = sort_direction;
    index.sort_value = sort_value;
    index
}

pub fn find(key: &str, index: &Index) -> Result<Option<(u64, u64)>, std::io::Error>{
    let key_string = key.to_string();
    let location;
    if index.sort_direction == SortDirection::Ascending {
        if index.sort_value == SortValue::Character { 
            location = index.index.binary_search_by(|v| { v.0.partial_cmp(&key_string).expect("Couldn't compare values") });
        } else {
            location = index.index.binary_search_by(|v| {
                let v0_as_float = v.0.parse::<f64>().unwrap();
                let key_as_float = key_string.parse::<f64>().unwrap();
                v0_as_float.partial_cmp(&key_as_float).expect("Couldn't compare values")
            });
        }
    } else {
        if index.sort_value == SortValue::Character { 
            location = index.index.binary_search_by(|v| { key_string.partial_cmp(&v.0).expect("Couldn't compare values") });
        }
        else { 
            location = index.index.binary_search_by(|v| {
                let v0_as_float = v.0.parse::<f64>().unwrap();
                let key_as_float = key_string.parse::<f64>().unwrap();
                key_as_float.partial_cmp(&v0_as_float).expect("Couldn't compare values")
            });
        }
    }

    let index_rec = match location {
        Ok(loc) => Some((index.index[loc].1, index.index[loc].2)),
        Err(_i) => None,
    };
    Ok(index_rec)
}

pub fn write(dest_file: &str, index: &Index) -> Result<i32, std::io::Error>{
    let serialized_index = serde_json::to_string(index).unwrap();
    std::fs::write(dest_file, serialized_index)?;
    Ok(0)
}

pub fn read(source_file: &str) -> Result<Index, std::io::Error>{
    let serialized_index = String::from_utf8(std::fs::read(source_file).unwrap()).unwrap();
    let index: Index = serde_json::from_str(&serialized_index).unwrap();
    Ok(index)
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use crate::enums::sort_value::SortValue;
    use crate::enums::sort_direction::SortDirection;
    use crate::functions::memory;
    use crate::functions::index::sort;
    use crate::functions::index::write;
    use crate::functions::index::read;

    #[test]
    fn test_sort() {
        let json = r#"[
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 1, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 10, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 2, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 20, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 3, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 30, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            ]"#;
        let rec_index = memory::index(json, "$.enhance.id").unwrap();
        let mut index = sort(&rec_index, SortValue::Character, SortDirection::Ascending);
        assert_eq!(index.index[0].0, "1");
        assert_eq!(index.index[1].0, "10");
        assert_eq!(index.index[2].0, "2");
        assert_eq!(index.index[3].0, "20");
        assert_eq!(index.index[4].0, "3");
        assert_eq!(index.index[5].0, "30");
        assert_eq!(index.sort_direction, SortDirection::Ascending);
        assert_eq!(index.sort_value, SortValue::Character);

        index = sort(&rec_index, SortValue::Character, SortDirection::Descending);
        assert_eq!(index.index[0].0, "30");
        assert_eq!(index.index[1].0, "3");
        assert_eq!(index.index[2].0, "20");
        assert_eq!(index.index[3].0, "2");
        assert_eq!(index.index[4].0, "10");
        assert_eq!(index.index[5].0, "1");
        assert_eq!(index.sort_direction, SortDirection::Descending);
        assert_eq!(index.sort_value, SortValue::Character);

        index = sort(&rec_index, SortValue::Numeric, SortDirection::Ascending);
        assert_eq!(index.index[0].0, "1");
        assert_eq!(index.index[1].0, "2");
        assert_eq!(index.index[2].0, "3");
        assert_eq!(index.index[3].0, "10");
        assert_eq!(index.index[4].0, "20");
        assert_eq!(index.index[5].0, "30");
        assert_eq!(index.sort_direction, SortDirection::Ascending);
        assert_eq!(index.sort_value, SortValue::Numeric);

        index = sort(&rec_index, SortValue::Numeric, SortDirection::Descending);
        assert_eq!(index.index[0].0, "30");
        assert_eq!(index.index[1].0, "20");
        assert_eq!(index.index[2].0, "10");
        assert_eq!(index.index[3].0, "3");
        assert_eq!(index.index[4].0, "2");
        assert_eq!(index.index[5].0, "1");
        assert_eq!(index.sort_direction, SortDirection::Descending);
        assert_eq!(index.sort_value, SortValue::Numeric);
    }
     
    #[test]
    fn test_read_and_write() {
       let json = r#"[
           { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 1, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
           { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 10, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
           { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 2, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
           { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 20, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
           { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 3, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
           { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 30, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
           ]"#;
       let rec_index = memory::index(json, "$.enhance.id").unwrap();
       let index = sort(&rec_index, SortValue::Numeric, SortDirection::Ascending);
       write(".\\data\\test.index", &index).unwrap();
       let index2 = read(".\\data\\test.index").unwrap();
       let index_string = serde_json::to_string(&index).unwrap();
       let index2_string = serde_json::to_string(&index2).unwrap();
       assert_eq!(index_string, index2_string);
    }

    #[test]
    fn test_find() {
        let json = r#"[
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 1, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 10, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 2, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 20, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 3, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            { "mode": "Enhancement", "buff_debuff": "Any", "enhance": { "id": 30, "sub_id": -1 }, "schedule": "A", "multiplier": 1, "fx": null },
            ]"#;
        let rec_index = memory::index(json, "$.enhance.id").unwrap();
        let mut index = sort(&rec_index, SortValue::Numeric, SortDirection::Ascending);
        let mut location = super::find("1", &index).unwrap().unwrap();
        assert_eq!(location.0, 14);
        assert_eq!(location.1, 144);
        location = super::find("2", &index).unwrap().unwrap();
        assert_eq!(location.0, 305);
        assert_eq!(location.1, 435);
        location = super::find("3", &index).unwrap().unwrap();
        assert_eq!(location.0, 596);
        assert_eq!(location.1, 726);
        location = super::find("10", &index).unwrap().unwrap();
        assert_eq!(location.0, 159);
        assert_eq!(location.1, 290);
        location = super::find("20", &index).unwrap().unwrap();
        assert_eq!(location.0, 450);
        assert_eq!(location.1, 581);
        location = super::find("30", &index).unwrap().unwrap();
        assert_eq!(location.0, 741);
        assert_eq!(location.1, 872);

        let mut no_location = super::find("999", &index).unwrap();
        assert_eq!(no_location, None);

        index = sort(&rec_index, SortValue::Character, SortDirection::Ascending);
        location = super::find("1", &index).unwrap().unwrap();
        assert_eq!(location.0, 14);
        assert_eq!(location.1, 144);
        location = super::find("2", &index).unwrap().unwrap();
        assert_eq!(location.0, 305);
        assert_eq!(location.1, 435);
        location = super::find("3", &index).unwrap().unwrap();
        assert_eq!(location.0, 596);
        assert_eq!(location.1, 726);
        location = super::find("10", &index).unwrap().unwrap();
        assert_eq!(location.0, 159);
        assert_eq!(location.1, 290);
        location = super::find("20", &index).unwrap().unwrap();
        assert_eq!(location.0, 450);
        assert_eq!(location.1, 581);
        location = super::find("30", &index).unwrap().unwrap();
        assert_eq!(location.0, 741);
        assert_eq!(location.1, 872);

        no_location = super::find("999", &index).unwrap();
        assert_eq!(no_location, None);

        index = sort(&rec_index, SortValue::Numeric, SortDirection::Descending);
        location = super::find("1", &index).unwrap().unwrap();
        assert_eq!(location.0, 14);
        assert_eq!(location.1, 144);
        location = super::find("2", &index).unwrap().unwrap();
        assert_eq!(location.0, 305);
        assert_eq!(location.1, 435);
        location = super::find("3", &index).unwrap().unwrap();
        assert_eq!(location.0, 596);
        assert_eq!(location.1, 726);
        location = super::find("10", &index).unwrap().unwrap();
        assert_eq!(location.0, 159);
        assert_eq!(location.1, 290);
        location = super::find("20", &index).unwrap().unwrap();
        assert_eq!(location.0, 450);
        assert_eq!(location.1, 581);
        location = super::find("30", &index).unwrap().unwrap();
        assert_eq!(location.0, 741);
        assert_eq!(location.1, 872);

        no_location = super::find("999", &index).unwrap();
        assert_eq!(no_location, None);

        index = sort(&rec_index, SortValue::Character, SortDirection::Descending);
        location = super::find("1", &index).unwrap().unwrap();
        assert_eq!(location.0, 14);
        assert_eq!(location.1, 144);
        location = super::find("2", &index).unwrap().unwrap();
        assert_eq!(location.0, 305);
        assert_eq!(location.1, 435);
        location = super::find("3", &index).unwrap().unwrap();
        assert_eq!(location.0, 596);
        assert_eq!(location.1, 726);
        location = super::find("10", &index).unwrap().unwrap();
        assert_eq!(location.0, 159);
        assert_eq!(location.1, 290);
        location = super::find("20", &index).unwrap().unwrap();
        assert_eq!(location.0, 450);
        assert_eq!(location.1, 581);
        location = super::find("30", &index).unwrap().unwrap();
        assert_eq!(location.0, 741);
        assert_eq!(location.1, 872);

        no_location = super::find("999", &index).unwrap();
        assert_eq!(no_location, None);
    }
}
