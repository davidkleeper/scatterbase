use std::io::prelude::*;
use std::fs::File;
use serde_json::Value;

pub fn index(json: &mut File, key_path: &str) -> Result<Vec::<(String, u64, u64)>, std::io::Error>{
    let mut layout = Vec::<(String, u64, u64)>::new();
    let (character, count) = read1(json);
    if 0 == count { return Ok(layout); }
    if '[' != character && '{' != character { return Ok(layout); }
    let mut current_character: usize = 1;
    let mut brace_count: usize = 0;
    let mut record_start: usize = 0;
    let mut record = Vec::<char>::new();
    if '{' == character { 
        record.push(character);
        brace_count = 1; 
    }
    loop {
        let (character, count) = read1(json);
        if 0 == count { return Ok(layout); }

        if '{' == character { 
            brace_count += 1;
            if 1 == brace_count { 
                record_start = current_character;
                record = Vec::<char>::new();
            }
            record.push(character);
        }
        else if '}' == character { 
            brace_count -= 1; 
            record.push(character);
            if 0 == brace_count { 
                let rec: String = record.iter().collect();
                let json_object = serde_json::from_str(rec.as_str())?;
                let mut selector = jsonpath::selector(&json_object);
                let keys = selector(key_path).unwrap();
                for keys_index in 0..keys.len() {
                    let key = match keys[keys_index] {
                        Value::Number(n) => format!("{}", n),
                        Value::String(s) => format!("{}", s),
                        _ => String::from("")
                    };
                    layout.push((key, record_start as u64, current_character as u64));
                    println!("Index #{}", layout.len());
                }
            }
        }
        else{
            record.push(character);
        } 
        current_character += count;
    }
}

fn read1(file: &mut File) -> (char, usize){
    let mut buffer = [0; 1];
    let n = match file.read(&mut buffer[..]) {
        Ok(count) => count,
        Err(_e) => 0
    };
    let c: char = buffer[0] as char;
    (c, n)
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {
    use std::fs::File;
    use crate::functions::disk::index;
    use crate::functions::cross_platform::get_new_line_length;
    use crate::functions::cross_platform::path;

    #[test]
    fn test_index() {
        let mut json = File::open(path("./data/empty.json")).unwrap();
        let mut rec_index = index(&mut json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 0);

        json = File::open(path("./data/empty-obj.json")).unwrap();
        rec_index = index(&mut json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 0);

        json = File::open(path("./data/empty-array.json")).unwrap();
        rec_index = index(&mut json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 0);

        json = File::open(path("./data/simple-obj.json")).unwrap();
        rec_index = index(&mut json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 1);
        assert_eq!(rec_index[0].0, "14");
        assert_eq!(rec_index[0].1, 0);
        assert_eq!(rec_index[0].2, 25);

        let mut json = File::open(path("./data/simple-array.json")).unwrap();
        let mut rec_index = index(&mut json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 1);
        assert_eq!(rec_index[0].0, "14");
        assert_eq!(rec_index[0].1, 1);
        assert_eq!(rec_index[0].2, 26);

        json = File::open(path("./data/simple-array-2.json")).unwrap();
        rec_index = index(&mut json, "$.id").unwrap();
        assert_eq!(rec_index.len(), 3);
        assert_eq!(rec_index[0].0, "14");
        assert_eq!(rec_index[0].1, 1);
        assert_eq!(rec_index[0].2, 26);
        assert_eq!(rec_index[1].0, "15");
        assert_eq!(rec_index[1].1, 29);
        assert_eq!(rec_index[1].2, 54);
        assert_eq!(rec_index[2].0, "16");
        assert_eq!(rec_index[2].1, 57);
        assert_eq!(rec_index[2].2, 82);

        json = File::open(path("./data/nested-obj.json")).unwrap();
        rec_index = index(&mut json, "$.enhance.id").unwrap();
        assert_eq!(rec_index.len(), 1);
        assert_eq!(rec_index[0].0, "14");
        assert_eq!(rec_index[0].1, 0);
        assert_eq!(rec_index[0].2, 131);

        json = File::open(path("./data/array-of-nested-obj.json")).unwrap();
        rec_index = index(&mut json, "$.enhance.id").unwrap();
        assert_eq!(rec_index.len(), 3);
        assert_eq!(rec_index[0].0, "14");
        assert_eq!(rec_index[0].1, 5 + get_new_line_length());
        assert_eq!(rec_index[0].2, 136 + get_new_line_length());
        assert_eq!(rec_index[1].0, "15");
        assert_eq!(rec_index[1].1, 142 + (get_new_line_length() * 2));
        assert_eq!(rec_index[1].2, 273 + (get_new_line_length() * 2));
        assert_eq!(rec_index[2].0, "16");
        assert_eq!(rec_index[2].1, 279 +  (get_new_line_length() * 3));
        assert_eq!(rec_index[2].2, 410 + (get_new_line_length() * 3));

        // json = File::open(path(".\\data\\powers.json")).unwrap();
        // rec_index = index(&mut json, "$.full_name").unwrap();
        // assert_eq!(rec_index.len(), 10347);
    }
}
