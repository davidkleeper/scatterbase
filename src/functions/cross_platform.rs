pub fn get_new_line_length() -> u64 {
    if cfg!(windows) {
        return 2;
    } 
    1
}

pub fn path(path: &str) -> String {
    if cfg!(windows) {
        return str::replace(&str::replace(path, "/", "\\"), "$PLATFORM$", "windows")
    } 
    return str::replace(&str::replace(path, "\\", "/"), "$PLATFORM$", "unix")
}

// cargo test -- --nocapture --test-threads=1
#[cfg(test)]
mod tests {

    #[test]
    fn test_get_new_line_length() {
        if cfg!(windows) {
            assert_eq!(super::get_new_line_length(), 2);
        } else {
            assert_eq!(super::get_new_line_length(), 1);
        }
    }

    #[test]
    fn test_path() {
        if cfg!(windows) {
            assert_eq!(super::path("./$PLATFORM$/data/empty.json"), ".\\windows\\data\\empty.json");
            assert_eq!(super::path(".\\$PLATFORM$\\data\\empty.json"), ".\\windows\\data\\empty.json");
        } else {
            assert_eq!(super::path("./$PLATFORM$/data/empty.json"), "./unix/data/empty.json");
            assert_eq!(super::path(".\\$PLATFORM$\\data\\empty.json"), "./unix/data/empty.json");
        }
    }
}
