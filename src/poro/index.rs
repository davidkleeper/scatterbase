use serde::{Serialize, Deserialize};

use crate::enums::sort_direction::SortDirection;
use crate::enums::sort_value::SortValue;

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Index{
    pub index: Vec::<(String, u64, u64)>,
    pub sort_direction: SortDirection,
    pub sort_value: SortValue,
}

#[allow(dead_code)]
impl Index {
    pub fn new() -> Index {
        let registry = Index {
            index: Vec::<(String, u64, u64)>::new(),
            sort_direction: SortDirection::Ascending,
            sort_value: SortValue::Character,
        };
        registry
    }
}
