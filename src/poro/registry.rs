use serde::{Serialize, Deserialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Registry{
    pub disk: HashMap<String, (String, String)>,
    pub memory: HashMap<String, (String, String)>,
}

#[allow(dead_code)]
impl Registry {
    pub fn new() -> Registry {
        let registry = Registry {
            disk: HashMap::<String, (String, String)>::new(),
            memory: HashMap::<String, (String, String)>::new(),
        };
        registry
    }
}
